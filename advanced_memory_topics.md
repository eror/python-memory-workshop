# Advanced memory topics
## Object interning
### Introduction 
#### Objects and variables - general rule
  * Objects are allocated on assignment (e.g. `a = "spam", b = 3.2`).
  * Variables just point to objects (i.e. they do not hold the memory).
#### Interning of Objects
  * This is an exception to the general rule.
  * Python implementation specific (examples from CPython).
  * "Often" used objects are preallocated and are shared instead of costly new alloc.  
  * Mainly due to the performance optimization.
##### Warning
  * This is Python implementation dependent.
  * This may change in the future.
  * This is not documented because of the above reasons.
  * For reference consult the source code.
##### CPython 2.7 - 3.4  - Single instances for:
  * int  - in range `[-5, 257)`
  * str / unicode  - empty string and all `length=1` strings
  * unicode / str -  empty string and all `length=1` strings for Latin-1
  * tuple - empty tuple

### Exercise
#### Ex.1 Something simple - compare code
```python 
a = 0; b = 0 
a is b, a == b
```
and 
```python 
a = 1024; b = 1024
a is b, a == b
```
Play with other object types
#### Ex.2 Memory footprint - measure maximal memory usage of two examples
```python 
a = [ i % 257 for i in xrange(2**20)]
```
and 
```python
b = [ 1024 + i % 257 for i in xrange(2**20)]
```
Play with other object types

## String Interning
### Introduction
   String interning is a method of storing only one copy of each distinct string value, which must be immutable. 
### From Cpython documentation: intern (py-2.x) / sys.intern (py-3.x)
  * Enter string in the table of “interned” strings.
  * Return the interned string (string or string copy).
  * Useful to gain a little performance on dictionary lookup (key comparisons after hashing can be done by a pointer compare instead of a string compare).
  * Names used in programs are automatically interned 
  * Dictionaries used to hold module, class or instance attributes have interned keys.

### Exercise
#### Ex.1 Something simple - analyse example
```python
a, b = "strin", "string"
a + 'g' is b 
intern(a+'g') is intern(b)
```
#### Ex.2 Memory footprint - measure maximal memory usage of two examples
```python
a = [ "spam %d" % (i % 257) for i in xrange(2**20)] 
```
and
```python
a = [ intern("spam %d" % (i % 257)) for i in xrange(2**20)] 
```
Play with the example ;).
#### Ex.3 Warning for `intern` usage - measure maximal memory usage and processing of two examples 
```python
x = []
for i in xrange(2**16):
  x.append("a"*i)
```
and 
```python
x = []
for i in xrange(2**16):
  x.append(intern("a"*i))
```
#### Ex.3 Warning for `intern` usage - measure snapshot memory usage of two examples 
```python
import meminfo as m
m.print_meminfo()
x = []
for i in xrange(2**16):
  x.append("a"*i)

del x
m.print_meminfo()
```
and
```python
import meminfo as m
m.print_meminfo()
x = []
for i in xrange(2**16):
  x.append(intern("a"*i))

del x
m.print_meminfo()
```

## Mutable Containers Memory Allocation Strategy 
### Introduction 
This section will explain basics of memory strategy for containers. Examples for lists and dictionaries will be provided.

Mutable Containers (List, Sets, Dictionaries) Memory Allocation Strategy:

  * Plan for growth and shrinkage
      * Slightly overallocate memory neaded by container.
      * Leave room to growth.
      * Shrink when overallocation threshold is reached.
  * Reduce number of expensive function calls:
      * `relloc()`
      * `memcpy()` 
  * Use optimal layout.

### List allocation strategy
  * Represented as fixed-length array of pointers.
  * Overallocation for list growth (by append)
    * List size growth: `4, 8, 16, 25, 35, 46,...`
    * For large lists less then 12.5% 
  * Due to the memory actions involved, operations:
    * at end of list are cheap (rare realloc), ...
    * in the middle or beginning require memory copy or shift!
  * Note that for small lists `1,2,5 elements`, space is wasted.
  * List allocation size: 
    * 32 bits - `32 + (4 * length)`
    * 64 bits - `72 + (8 * length)`
  * Shrinking only when (`list size < 2`) of allocated space.

### Dictionaries allocation strategy
  * Represented as fixed-length hash tables.
    * Overallocation for dict/sets - when 2/3 of capacity is reached. 
      * if `number of elements < 50000`: quadruple the capacity 
      * else: double the capacity 
```c
// dict growth strategy
(mp->ma_used>50000 ? 2 : 4) * mp->ma_used;
// set growth strategy
so->used>50000 ? so->used*2 : so->used*4);
```
  * Dict/Set growth/shrink code
```c
for (newsize = PyDict_MINSIZE;
newsize <= minused && newsize > 0;
newsize <<= 1);
```
  * Shrinkage if dictionary/set fill (real and dummy elements) is much larger than used elements (real elements) i.e. lot of keys have been deleted.

### Exercise
#### Ex. 1. List allocation strategy
  * Create empty list x
  * In a loop increase its size using append (iterate over `xrange(2**10)`)
  * At each step of loop print sys.getsizeof(x)
  * Compare results with above described allocation strategy
#### Ex. 2. Repeat Ex. 1. for dicts and sets
#### Ex. 3. Performance using list
  * create large list of integers (`2**20`)
    * first approach use append(i)
    * second approach use insert(0, i)
  * compare results

## Notes with examples on Python garbage collector, reference count and cycles.
### Introduction 
#### Python garbage collector:
  * Uses reference counting.
  * Offers cycle detection.
  * Objects garbage-collected when count goes to 0.
  * Reference increment, e.g.: object creation, additional aliases, passed to function
  * Reference decrement, e.g.: local reference goes out of scope, alias is destroyed, alias is reassigned

#### Warning - from documentation
Objects that have `__del__()` methods and are part of a reference cycle cause the entire reference cycle to be uncollectable!
  * Python doesn’t collect such cycles automatically.
  * It is not possible for Python to guess a safe order in which to run the `__del__()` methods. 
#### Jython
Uses the JVM's built-in garbage collection - so no need to copy cPython's reference-counting implementation.
#### PyPy
 * Supports pluggable garbage collectors - so various GC avalible.
 * Default incminimark which does "major collections incrementally (i.e. one major collection is split along some number of minor collections, rather than being done all at once after a specific minor collection)"

### Exercise
#### Ex. 1. Prepare simple cycle between two objects
  * prepare two objects with reference cycle between
  * delete both objects using `del` for example
  * investigate using using `gc`
```python
import gc
print gc.collect()
print gc.garbage
```
#### Ex. 2. Prepare uncollectable garbage
  * Repeat Ex. 1, but this time override `__del__` method (pass will be enough)
#### Ex. 3. Differences in garbage collector among various Python implementations - PyPy, Stackless Python, Jython
  * Rerun previous examples using pypy, jython, slpython2.7.

