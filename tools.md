# Tools for debugging memory in Python

## Getting info on the OS process: `psutil`
* Home page: https://pythonhosted.org/psutil/ 

The `psutil` package offers API for accessing data about operating system
utilization and processes. It’s quite complete and has comprehensive
documentation. Works on many platforms on Python 2.4 to 3.4.

With this package you can easily access information about any process memory
usage, as seen from OS perspective. Anything available to `ps` and `top`
commands should be also available programatically with `psutil`.

Use this library to implement features like checking if process uses more
memory then expected, getting information about process children, monitoring
state of the whole OS. Consider it also when writing programs with
communicating with sub-processes via `Popen`.

## Tracking line by line memory usage: `memory_profiler`
* Home page: https://pypi.python.org/pypi/memory_profiler

Sometimes you find that your program just ate all memory available. This tool
will help you pinpoint where exactly this has happened by displaying each line
your program executes along with memory usage and difference. You can even
instruct it to run you program normally until it reaches certain memory level.
When this happens `memory_profiler` will drop you into `pdb` shell.

Documentation is not comprehensive, but on other hand it’s a simple tool and
does not need much of explanation. It should run  python 2 & 3, possibly
cross-platform (it depends on psutil to do that).

## New kid on the block: `tracemalloc`
* Home page: https://docs.python.org/3/library/tracemalloc.html

Python 3.4 comes with a real good module: `tracemalloc`. It’s sits somewhere
between `memory_profiler` and `heapy`: it allows you to, unsurprisingly, trace
memory allocations. That means that using this you can pinpoint lines or files
that allocate most memory, either line-by-line or by tracing memory blocks to
lines that allocated them. It also uses the same internals as the interpreter,
so it allows you to do the tracing even during interpreter startup.

As part of the standard library, it avoids installation problems and has good
documentation. It’s also probably most trustworthy of tools shown: it should
change with Python itself and will not be left behind (hopefully). There’s
backport available for older Pythons, but you’d have to patch and recompile
Python to use it.

## Object relation graphs: `objgraph`
* Home page: http://mg.pov.lt/objgraph/

Interesting tool that allows you to track and visualise how object reference
each other. This will help you track problems with cyclic references. Be aware
though, it works best with selected, small samples of objects. For bigger
collections it will produce huge and unreadable graphs.

It has good documentation and works on Python 2 & 3.

## Looking what’s under the hood: `guppy` & `heapy`
* Home page: http://guppy-pe.sourceforge.net/

`Heapy` might be most interesting and most helpful tool: it can browse through
all memory Python is aware and display some statistics. It’s very powerful, but
it has flaws. Once you start using it, it calls garbage collection multiple
times. It also uses a some memory by itself and just using it might change
memory properties of you program. Documentation is dismal, works only on Python
2.7 and API is not very intuitive. It’s still damn useful: knowing the
difference between what interpreter claims your program uses and what system
say it uses can certainly make huge difference in debugging.
