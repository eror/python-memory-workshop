# This short program showcases contrived use of cyclic references
# in data structures. We construct a binary tree suitable for
# parent -> child and child -> parent traversals.
#
# Exercise:
# 1. Use memory_profiler to confirm that memory is not released to the
# system after call to `print_tree`.
#
# 2. Use `objgraph` to discover that some after the `print_tree` call
# some `Node` objects still have some references. Try generating graph
#
# 3. Under Python 3.4 try using `tracemalloc` from standard library
# to pinpoint place where the most memory is allocated.
#
# 4. Try couple of fixes: forcing GC run, breaking reference cycles.
#
# 5. Try adding __del__ method to `Node` class and observe that
# gc can't collect objects.
#
from __future__ import unicode_literals, print_function, absolute_import
import gc
import objgraph


class Stub(object):
    # Stub is Null-Object for Node
    level = 0

    def traverse_in_order(self):
        return ()

    def traverse_pre_order(self):
        return ()

    def traverse_post_order(self):
        return ()


class Node(object):
    # Represents single node in tree
    def __init__(self, value, left=None, right=None):
        self.padding = b'a' * 10000000  # make leak more visible
        self.value = value
        self.parent = Stub()
        if left:
            self.left = left
            self.left.parent = self  # setup back reference
        else:
            self.left = Stub()
        if right:
            self.right = right
            self.right.parent = self  # setup back reference
        else:
            self.right = Stub()

    def traverse_in_order(self):
        for node in self.left.traverse_in_order():
            yield node
        yield self
        for node in self.right.traverse_in_order():
            yield node

    def traverse_pre_order(self):
        yield self
        for node in self.left.traverse_pre_order():
            yield node
        for node in self.right.traverse_pre_order():
            yield node

    def traverse_post_order(self):
        for node in self.left.traverse_post_order():
            yield node
        for node in self.right.traverse_post_order():
            yield node
        yield self

    def __repr__(self):
        return 'Node: %s' % self.value

    @property
    def level(self):
        return 1 + self.parent.level

    @classmethod
    def from_dict(cls, data):
        # Recursively builds tree from dict
        left, right = None, None
        if 'left' in data:
            left = cls.from_dict(data['left'])
        if 'right' in data:
            right = cls.from_dict(data['right'])
        value = data['value']
        return cls(value, left=left, right=right)


def print_tree():
    # Builds an artificial tree and prints its contents
    tree_data = {
        'value': 'a',
        'left': {
            'value': 'b',
            'left': {
                'value': 'd'
            },
        },
        'right': {
            'value': 'c',
            'left': {
                'value': 'e',
                'right': {
                    'value': 'g'
                }
            },
            'right': {
                'value': 'f'
            }
        }
    }
    tree = Node.from_dict(tree_data)
    for node in tree.traverse_pre_order():
        msg = '%s - %d' % (node.value, node.level)
        print(msg)


def main():
    print_tree()


if __name__ == '__main__':
    main()
