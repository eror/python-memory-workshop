# This simple program simulates simple producer-consumer scheme.
# Consumers will quickly grow in size.
#
# Exercise:
# Use `psutil` to implement simple pooling mechanisms:
# re-spawning of dead processes and killing processes that are using
# too much memory.
from __future__ import unicode_literals, print_function, absolute_import
import time
import random
import psutil
import multiprocessing as mp

# This cache is not shared between processes
cache = {}


def alloc(size=1):
    # Allocates roughly `size` kilobytes
    return b'x' * 1000 * size


def consume(name, queue):
    # Waits for keys from queue to allocate cache at given key.
    # Will rapidly grow child process over time.
    while True:
        cache_key = queue.get()
        print('[%s] Working...' % name)
        payload = alloc(1000)
        cache[cache_key] = payload


def main():
    # Starts group of worker processes and keeps producing tasks
    # for the queue.
    queue = mp.Queue()
    pool = []
    for i in range(5):
        p = mp.Process(target=consume, args=[i, queue])
        p.start()
        pool.append(p)

    while True:
        key = random.randrange(1000)
        queue.put(key)
        time.sleep(0.1)


if __name__ == '__main__':
    main()
