# Exercise:
# Try running this under cPython 2.7 and see if you can explain
# the memory behaviour.
from __future__ import print_function, unicode_literals
import gc
from lib import print_rss
import random

LETTERS = b'abcdefghijklmnopqrs'


def main():
    big = []
    for i in range(500):
        # Allocate longer and longer lists
        strings = [random.choice(LETTERS) * 500 for i in range(i)]
        big.extend(strings)
    print_rss('After work')


if __name__ == '__main__':
    print_rss('Before main')
    main()
    print_rss('After main')
