# This example shows that in cPython 2.7 it's not wise to create big
# lists of ints: interpreter keeps special memory set aside just for
# that. And the best: it's reused, but never freed.
from __future__ import unicode_literals, print_function, absolute_import
import gc
import lib

MILLION = 1000000


def main():
    ints = range(10 * MILLION)
    for i in ints:
        if i % MILLION == 0:
            print(i)


if __name__ == '__main__':
    lib.print_rss()
    main()
    lib.print_rss()
