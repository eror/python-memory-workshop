# Running this in python3 leaves lot's of memory unreleased
from __future__ import print_function, unicode_literals
import gc
from lib import print_rss

try:
    xrange
except NameError:  # PY3
    xrange = range


def main():
    work_objects = []
    for run_id in xrange(1, 100):
        chunks = [{} for i in xrange(100000 // run_id)]
        chunks = chunks[::2]
        work_objects.extend(chunks)
    print_rss('After work')


if __name__ == '__main__':
    print_rss('Before main')
    main()
    print_rss('After main')
