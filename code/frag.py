# This program simulates common pattern of making some memory-intensive
# work and returning result. Unexpectedly, memory is not released immediately
# after work.
#
# Exercise 1:
# Try to playing with best element selection, see how this affects perceived
# memory usage.
#
# Excercise 2:
# Try running this example with `jemalloc`
#
from __future__ import unicode_literals, print_function, absolute_import
import gc
from lib import print_rss

try:
    xrange
except NameError:  # PY3
    xrange = range


def work():
    # Makes "calculations" using lots of memory
    work_list = [b'a' * 2000 for i in xrange(100000)]
    print_rss('After alloc')
    best = work_list[-1]
    return best


def main():
    result = work()
    print(result[:5])
    print_rss('After work')


if __name__ == '__main__':
    main()
