from __future__ import unicode_literals, print_function, absolute_import
import six
import psutil
import contextlib


@contextlib.contextmanager
def report_sys_mem_change():
    """Prints out program memory usage as seen by system"""
    process = psutil.Process()
    start_rss, start_vms = process.memory_info()
    yield
    end_rss, end_vms = process.memory_info()
    rss = (end_rss - start_rss) // 1024
    vms = (end_vms - start_vms) // 1024
    print('RSS: %+dkB  VMS: %+dkB' % (rss, vms))


if six.PY3:
    import tracemalloc

    @contextlib.contextmanager
    def report_py_mem_change():
        """Prints out program memory usage as seen by Python interpreter"""
        tracemalloc.start()
        start = tracemalloc.take_snapshot()
        yield
        stop = tracemalloc.take_snapshot()
        stats = stop.compare_to(start, 'filename', cumulative=True)
        size = sum(diff.size_diff // 1024 for diff in stats)
        count = sum(diff.count_diff for diff in stats)
        print('Size: %+dkB  Objects: %+d' % (size, count))
        tracemalloc.stop()


else:
    try:
        import guppy

        hp = guppy.hpy()

        @contextlib.contextmanager
        def report_py_mem_change():
            """Prints out program memory usage as seen by Python interpreter"""
            heap = hp.heap()
            start_size, start_count = heap.size, heap.count
            yield
            heap = hp.heap()
            end_size, end_count = heap.size, heap.count
            size = (end_size - start_size) // 1024
            count = end_count - start_count
            print('Size: %+dkB  Objects: %+d' % (size, count))
    except ImportError:
        # guppy does not compile under pypy :-(

        @contextlib.contextmanager
        def report_py_mem_change():
            yield


def print_rss(text='RSS'):
    process = psutil.Process()
    mem = process.memory_info()
    print(text, mem.rss // 1024, 'kB')
