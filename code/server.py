# This is simple program that will work on in-memory cache.
# Cache is initialized with roughly 1000 megabytes and, if left
# unattended, will grow to more than 3000 megabytes.
#
# Exercise:
# Use `psutil` library to check process memory use and keep it mostly
# below 1200 megabytes.
# 
from __future__ import unicode_literals, print_function, absolute_import
import time
import psutil
import random

cache = []


def alloc(size=1):
    # Allocates roughly `size` kilobytes
    return b'x' * 1000 * size


def init_cache():
    # Fills cache with roughly 1000 megabytes
    for x in xrange(1000):
        payload = alloc(1000)
        cache.append(payload)


def churn():
    # Replaces random cache element with bigger one
    print('Working cache: %d.' % len(cache))
    index = random.randrange(len(cache))
    new_size = random.randint(1000, 3500)
    payload = alloc(new_size)
    cache[index] = payload


if __name__ == '__main__':
    init_cache()
    while True:
        churn()
        time.sleep(0.1)
