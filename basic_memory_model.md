# Basic objects memory representation - what is the actual size of basic types, how to check it, various types features
## sys.getsizeof(obj)
### Intoduction
From documentation:

  * Since Python 2.6
  * Return the size of an object in bytes.  The object can be any type. 
  * All built-in objects will return correct results. 
  * May not be true for third-party extensions as it is implementation specific.
  * Calls the object’s __sizeof__ method and adds an additional garbage collector overhead if the object is managed by the garbage collector.

### Exercise
#### Ex. 1. Prepare code for checking basic types sizes:
  * `int (py-2.7), long (py-2.7) / int (py-3.3), float, complex, str (py-2.7) / bytes (py-3.3)`
#### Ex. 2. getsizeof will work on any type, use it on different containers, experiment with different number of elements in a container
  * `dict, list, tuple`
#### Ex. 3. Note that getsizeof returns the size of container object and not the size of data associated with this container, experiment with the following code
```python
a =[ "Foo"*100, "Bar"*100, "SpamSpamSpam"*100]
b = [1,2,3]
print sys.getsizeof(a), sys.getsizeof(b)
```
## Different data representations and how they affect memory consumption - this will cover:
### Introduction
Your task is to process a large portion of data structures. 
Consider different representation forms of this data in Python, as this may drastically influence the used memory.
### Exercise
#### Ex. 1. Start by preparing various representations
Data structure:
```
LastName, FirstName, Address, City, PostalCode
```
Consider following data representations:
```python
class OldStyleClass: #only py-2.x
    ...
class NewStyleClass(object): # default for py-3.x
    ...
class NewStyleClassSlots(object):
    __slots__ = ('field1', 'field2', ...)
    ...
import collections as c
NamedTuple = c.namedtuple('nt', [ 'field1', ...,])

TupleData = ('value1', 'value2', ....)
ListaData = ['value1', 'value2', ....]
DictData = {'field1':, 'value2', ....}
```
#### Ex. 2. Prepare code that will initialize a large number of objects in given representation
  * This will be used to compare various representations memory footprint
  * We want this to be maximally independent so `one process = one measured representation` e.q. `./represntation.py NamedTuple` 
  * Preferably create exactly the same data for each representation

#### Ex. 3. Measure the representations
Use one of described methods (in presentation) to measure the memory footprint, for e.q.:
  
  * `/usr/bin/time -v ./representation NamedTuple to measure the maximum memory used.`
  * `valgrind --trace -children=yes --tool=massif python src.py && ms_print massif.out.PID`

# Differences in basic objects representations among various Python implementations - PyPy, Stackless Python, Jython
Rerun previous examples using pypy, jython, slpython2.7.
Use any suitable profiling tool (psutil is not be suitable).

Don't be so hard on jython ;) - Remember it has to start whole jvm.
