# Code exercises

## Virtual machine
You should have a virtual machine provided by workshop organisers. This is
fresh Ubuntu 14.04 (64 bit) with 2 GB of RAM. You can either use graphical
interface provided by Virtualbox or login through SSH on `localhost`, port
2222, user `memory`, password `memory`: 

		ssh memory@localhost -p 2222 # pass: memory

You will find various Python interpreters installed: cPython 2.6, 2.7.3, 2.7.6,
3.4; PyPy 2.4 (implements Python 2.7.8); Stackless Python 2.7, Jython. There is
bunch of virtualenvs with various memory tools already installed. 

Machine also has alternative malloc implementations. To run a program with
`tcmalloc` or `jemalloc` just do:

	with_tcmalloc ~/py3/bin/python program.py
	with_jemalloc ~/py3/bin/python program.py

There’s also `htop`, which is a better `top` that has better user interface.

## Simple “server”
* File: `code/server.py`

This is simple program that will work on in-memory cache. Cache is initialized
with roughly 1000 megabytes and, if left unattended, will grow to more than
3000 megabytes.

### Exercises
1. Use `psutil` library to check process memory use and keep it mostly below
1200 megabytes.
2. See what’s the impact of using `tcmalloc` and `jemalloc` on this  one,
especially with previous task finished.

## Processor
* File: `code/processor.py`

This simple program simulates simple producer-consumer scheme. Consumers will
quickly grow in size.

### Exercises
1. Use `psutil` to implement simple pooling mechanisms: re-spawning of dead
processes and killing processes that are using too much memory.
2. Again, try `tcmalloc` and `jemalloc`.

## Tree
* File: `code/trees.py`

This short program showcases contrived use of cyclic references
in data structures. We construct a binary tree suitable for
parent -> child and child -> parent traversals.

### Exercises:
1. Use memory_profiler to confirm that memory is not released to the system
after call to `print_tree`.

2. Use `objgraph` to discover that some after the `print_tree` call
some `Node` objects still have some references. Try generating graph of object
relations.

3. Under Python 3.4 try using `tracemalloc` from standard library to pinpoint
place where the most memory is allocated.

4. Try couple of fixes: forcing GC run, breaking reference cycles.

5. Try adding __del__ method to `Node` class and observe that
garbage collector can't collect objects anymore.


## Integer pools
* File: `code/pools.py`

This example shows that in cPython 2.7 it's not wise to create big lists of
ints: interpreter keeps special memory set aside just for that. And the best:
it's reused, but never freed.

### Excersises
1. Try this program under various interpreters, observe behaviour.

## Work memory fragmentation
* File: `code/frag.py`

This program simulates common pattern of making some memory-intensive work and
returning result. Unexpectedly, memory is not released immediately after work.

### Exercises:
1. Try to playing with best element selection, see how this affects perceived
memory usage. Use `heapy` and `tracemalloc` to try to pinpoint where memory
leaks.

2. Try running this example with `jemalloc`.

## Other memory fragmentation
File: `code/frag2.py`

Although Python 3 is generally better at handling memory and releasing it, this
example show’s that it’s not always true.

### Exercises
1. Run this on Python 3
2. Try `tracemalloc` to see if you can spot any memory leaks

## Tricky lists
Simple program that on Python 2.7 does not release memory.

### Exercise 
1. Try to figure out what’s happening
