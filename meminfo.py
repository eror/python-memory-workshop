import psutil
import sys
import os

def convert_bytes(n):
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n

def print_meminfo():
    try:
        p = psutil.Process(os.getpid())
        pinfo = p.as_dict()
    except psutil.NoSuchProcess:
        sys.exit(str(sys.exc_info()[1]))

    mem = '%s%% (resident=%s, virtual=%s) ' % (
            round(pinfo['memory_percent'], 1),
            convert_bytes(pinfo['memory_info'].rss),
            convert_bytes(pinfo['memory_info'].vms))
    print (mem)
